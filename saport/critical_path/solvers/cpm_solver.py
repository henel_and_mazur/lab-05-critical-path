import networkx as nx
from ..model import Project
from ..project_network import ProjectState, ProjectNetwork
from typing import List, Dict
from ..solution import FullSolution


class Solver:
    '''
    A "critical path method" solver for the given project.

        Attributes:
    ----------
    project_network: ProjectNetwork
        a project network related to the given project

    Methods:
    --------
    __init__(problem: Project):
        create a solver for the given project
    solve -> FullSolution:
        solves the problem and returns the full solution
    forward_propagation() -> Dict[ProjectState,int]:
        calculates the earliest times the given events (project states) can occur
        returns a dictionary mapping network nodes to the timestamps
    backward_propagation(earliest_times: Dict[ProjectState, int]) -> Dict[ProjectState,int]:
        calculates the latest times the given events (project states) can occur
        uses earliest times to start the computation
        returns a dictionary mapping network nodes to the timestamps
    calculate_slacks(earliest_times: Dict[ProjectState, int], latest_times: Dict[ProjectState,int]) -> Dict[str, int]:
        calculates slacks for every task in the project
        uses earliest times and latest time of the events in the computations
        returns a dictionary mapping tasks names to their slacks
    create_critical_paths(slacks: Dict[str,int]) -> List[List[str]]:
        finds all the critical paths in the project based on the tasks' slacks
        returns list containing paths, every path is a list of tasks names put in the order they occur in the critical path 
    '''
    def __init__(self, problem: Project):
        self.project_network = ProjectNetwork(problem)

    def solve(self) -> FullSolution:
        earliest_times = self.forward_propagation()
        latest_times = self.backward_propagation(earliest_times)
        task_slacks = self.calculate_slacks(earliest_times, latest_times)
        critical_paths = self.create_critical_paths(task_slacks)
        # TODO:
        # set duration of the project based on the gathered data
        duration = 0

        for taskName in self.project_network.project.tasks.keys():
            for i in range(len(critical_paths[0])):
                if str(critical_paths[0][i]) == str(taskName):
                    duration += self.project_network.project.tasks[taskName].duration

        return FullSolution(duration, critical_paths, task_slacks)

    def forward_propagation(self) -> Dict[ProjectState, int]:
        # TODO:
        # earliest time of the project start node is always 0
        # every other event can occur as soon as all its predecessors plus duration of the tasks leading to the state
        #
        # earliest_times[state] = e
        earliest_times = {}
        earliest_times[self.project_network.start_node] = 0
        nodes = self.project_network.normal_nodes()
        # print("Poczatek")
        # print(nodes)
        # print("Koniec")
        for node in nodes:
            # if not node != self.project_network.start_node:
                wchodzace = self.project_network.predecessors(node)
                wybrany = None
                czasyWchodzacych = []
                for i in wchodzace:
                    czas = self.project_network.arc_duration(i, node) + earliest_times[i]
                    czasyWchodzacych.append(czas)
                    if czas >= max(czasyWchodzacych):
                        wybrany = i
                earliest_times[node] = self.project_network.arc_duration(wybrany, node) + earliest_times[wybrany]
                czasyWchodzacych.clear()
        wchodzaceDoOstatniego = self.project_network.predecessors(self.project_network.goal_node)
        czasyOst = []
        wybrany = None
        for i in wchodzaceDoOstatniego:
            czasOst = self.project_network.arc_duration(i, self.project_network.goal_node) + earliest_times[i]
            czasyOst.append(czasOst)
            if czasOst >= max(czasyOst):
                wybrany = i
        earliest_times[self.project_network.goal_node] = self.project_network.arc_duration(wybrany, self.project_network.goal_node) + earliest_times[wybrany]
        return earliest_times

    def backward_propagation(self, earliest_times: Dict[ProjectState, int]) -> Dict[ProjectState, int]:
        # TODO:
        # latest time of the project goal node always equals earliest time of the same node
        # every other event occur has to occur before its successors latest time
        latest_times = {}
        latest_times[self.project_network.goal_node] = earliest_times[self.project_network.goal_node]
        nodes = self.project_network.normal_nodes()
        nodes.reverse()
        # print("Od tylu")
        # print(nodes)
        # print("Koniec")
        for node in nodes:
            # if not node != self.project_network.goal_node:
                wychodzace = self.project_network.successors(node)
                wybrany = None
                czasyWychodzacych = []
                for i in wychodzace:
                    czas = latest_times[i] - self.project_network.arc_duration(node, i)
                    czasyWychodzacych.append(czas)
                    if czas <= min(czasyWychodzacych):
                        wybrany = i
                latest_times[node] = latest_times[wybrany] - self.project_network.arc_duration(node, wybrany)
                czasyWychodzacych.clear()
        wychodzaceZpierwszego = self.project_network.successors(self.project_network.start_node)
        czasyOst = []
        wybrany = None
        for i in wychodzaceZpierwszego:
            czasOst = latest_times[i] - self.project_network.arc_duration(self.project_network.start_node, i)
            czasyOst.append(czasOst)
            if czasOst <= min(czasyOst):
                wybrany = i
        latest_times[self.project_network.start_node] = latest_times[wybrany] - self.project_network.arc_duration(self.project_network.start_node, wybrany)
        return latest_times

    def calculate_slacks(self, 
                         earliest_times: Dict[ProjectState, int], 
                         latest_times: Dict[ProjectState, int]) -> Dict[str, int]:
        # TODO:
        # slack of the task equals "the latest time of its end" minus "earliest time of its start" minus its duration
        # tip: remember to ignore dummy tasks (task.is_dummy could be helpful)
        slacks = {}
        lista = self.project_network.edges()
        for i in lista:
            poczatek = earliest_times[i[0]]
            koniec = latest_times[i[1]]
            task = i[2]
            if not task.is_dummy:
                slacks[task.name] = koniec - poczatek - task.duration
        return slacks

    def create_critical_paths(self, slacks: Dict[str, int]) -> List[List[str]]:
        # TODO:
        # critical path start connects start node to the goal node
        # and uses only critical tasks (critical task has slack equal 0)
        # 1. create copy of the project network
        copiedNetwork = self.project_network.network.copy()
        # 2. remove all the not critical tasks from the copy
        for edge in self.project_network.edges():
            startingNode, terminalNode, task = edge
            if task.is_dummy != True:
                if task.name in slacks.keys():
                    if slacks[task.name] != 0:
                        copiedNetwork.remove_edge(startingNode, terminalNode)
                else:
                    copiedNetwork.remove_edge(startingNode, terminalNode)

        # 3. find all the paths from the start node to the goal node

        listOfPaths = []
        listOfNodes = []
        listOfNodes.append(self.project_network.start_node)
        listOfPaths.append(listOfNodes)

        endedPaths = 0
        while True:
            for i in range(len(listOfPaths)):
                nextNodesList = list(copiedNetwork.successors(listOfPaths[i][-1]))
                nextNodesCounter = len(nextNodesList)
                copiedPath = list(listOfPaths[i])
                if nextNodesCounter == 0:
                    if listOfPaths[i][-1].index != self.project_network.goal_node.index:
                        listOfPaths.pop(i)
                        break
                for j in range(nextNodesCounter):
                    nextNode = nextNodesList[j]
                    if j == 0:
                        listOfPaths[i].append(nextNode)
                        if self.project_network.goal_node.index == nextNode.index:
                            endedPaths += 1
                    else:
                        listOfPaths.append(list(copiedPath))
                        listOfPaths[-1].append(nextNode)
                        if self.project_network.goal_node.index == nextNode.index:
                            endedPaths += 1
            if len(listOfPaths) == endedPaths:
                break

        critical_paths = []
        for path in listOfPaths:
            tasksList = []
            for edge in self.project_network.edges():
                startingNode, terminalNode, task = edge
                for i in range(len(path)-1):
                    if startingNode.index == path[0+i].index and terminalNode.index == path[1+i].index:
                        if task.is_dummy != True:
                            tasksList.append(task.name)
            critical_paths.append(tasksList)

        # 4. translate paths (list of nodes) to list of tasks connecting the nodes
        #
        # tip 2. use method "remove_edge(<start>, <end>" directly on the graph object
        #        (e.g. self.project_network.network or rather its copy)
        # tip 3. nx.all_simple_paths method finds all the paths in the graph
        # tip 4. if "L" is a list "[1,2,3,4]", zip(L, L[1:]) will return [(1,2),(2,3),(3,4)]
        return critical_paths
