from ..model import Project
from ..project_network import ProjectNetwork
from ...simplex.model import Model
from ...simplex.expressions.expression import Expression
from ..solution import BasicSolution


class Solver:
    '''
    Simplex based solver looking for the critical path in the project.
    Uses linear model maximizing length of the path in the project network. 

    Attributes:
    ----------
    project_network: ProjectNetwork
        a project network related to the given project
    model: simplex.model.Model
        a linear model looking for the maximal path in the project network
    Methods:
    --------
    __init__(problem: Project)
        create a solver for the given project
    create_simplex_model() -> simplex.model.Model
        builds a linear model of the problem
    solve() -> BasicSolution
        finds the duration of the critical (longest) path in the project network
    '''
    def __init__(self, problem: Project):
        self.project_network = ProjectNetwork(problem)
        self.model = self.create_simplex_model()
    def create_simplex_model(self) -> Model:
        # TODO:
        # 0) we need as many variables as there is edges in the project network
        # 1) every variable has to be <= 1
        # 2) sum of the variables starting at the initial state has to be equal 1
        # 3) sum of the variables ending at the goal state has to be equal 1
        # 4) for every other node, total flow going trough it has to be equal 0
        #    i.e. sum of incoming arcs minus sum of the outgoing arcs = 0
        # 5) we have to maximize length of the path
        #    (sum of variables weighted by the durations of the corresponding tasks)
        model = Model("critical path (max)")
        edges = self.project_network.edges()
        for edge in edges:                                                                                         # (0)
            startingNode, terminalNode, task = edge
            t = model.create_variable(f't{startingNode.index}{terminalNode.index}')
        for i in range(len(model.variables)):                                                                      # (1)
            model.add_constraint(model.variables[i] <= 1)

        wychodzaceZpoczatku = list(self.project_network.successors(self.project_network.start_node) )              # (2)
        sumaWychodzacychZpoczatku = 1.0 * model.variables[0]
        if len(wychodzaceZpoczatku) >= 2:
            for i in range(len(model.variables)-1):
                for j in range(len(model.variables)):
                    if str(model.variables[i+1].name) == str(f"t{self.project_network.start_node.index}{j}"):
                        sumaWychodzacychZpoczatku += model.variables[i+1]

        model.add_constraint(sumaWychodzacychZpoczatku == 1)


        wchodzaceDoOstatniego = list(self.project_network.predecessors(self.project_network.goal_node))            # (3)
        sumaWchodzacychDoOstatniego = 1.0 * model.variables[-1]
        if len(wchodzaceDoOstatniego) >= 2:
            for i in range(len(model.variables)-1):
                for j in range(len(model.variables)):
                    if str(model.variables[-((i+1)+1)].name) == str(f"t{j}{self.project_network.goal_node.index}"):
                        sumaWchodzacychDoOstatniego += model.variables[-((i+1)+1)]

        model.add_constraint(sumaWchodzacychDoOstatniego == 1)


        for node in self.project_network.normal_nodes():
            sumaNodea = None
            edgeIndex = 0
            wychodzace = list(self.project_network.successors(node))
            wchodzace = list(self.project_network.predecessors(node))

            for edge in edges:
                for outp in wychodzace:
                    if sumaNodea is None:
                        if str(self.project_network.arc_task(node, outp).name) == str(edge[2].name):
                            sumaNodea = -1.0 * model.variables[edgeIndex]
                    else:
                        if str(self.project_network.arc_task(node, outp).name) == str(edge[2].name):
                            sumaNodea -= model.variables[edgeIndex]
                for inp in wchodzace:
                    if sumaNodea is None:
                        if str(self.project_network.arc_task(inp, node).name) == str(edge[2].name):
                            sumaNodea = 1.0 * model.variables[edgeIndex]
                    else:
                        if str(self.project_network.arc_task(inp, node).name) == str(edge[2].name):
                            sumaNodea += model.variables[edgeIndex]
                edgeIndex += 1

            if sumaNodea is not None:
                model.add_constraint(sumaNodea == 0)

        suma = None                                                                                                 #(5)
        index = 0
        for i in edges:
            if suma is None:
                suma = i[2].duration * model.variables[index]
            else:
                suma += i[2].duration * model.variables[index]
            index += 1

        model.maximize(suma)

        return model

    def solve(self) -> BasicSolution:
        solution = self.model.solve()
        return BasicSolution(int(solution.objective_value()))
